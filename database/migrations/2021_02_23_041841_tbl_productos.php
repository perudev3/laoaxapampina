<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mae_productos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_producto');
            $table->integer('id_categoria');
            $table->string('no_producto')->nullable();
            $table->decimal('pt_producto', 10.0)->nullable();
            $table->integer('qt_stock')->nullable();
            $table->string('tx_img')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mae_productos');
    }
}
