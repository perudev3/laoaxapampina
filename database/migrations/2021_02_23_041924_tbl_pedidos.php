<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_pedidos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_pedidos');
            $table->string('nu_compra')->nullable();
            $table->string('token_session')->nullable();
            $table->integer('id_producto')->nullable();
            $table->string('no_cliente')->nullable();
            $table->string('tl_cliente')->nullable();
            $table->string('mail_cliente')->nullable();
            $table->text('tx_direccion')->nullable();
            $table->date('fe_compra')->nullable();
            $table->time('hr_compra')->nullable();
            $table->string('tipo_pago')->nullable();
            $table->integer('est_pedido')->nullable();
            $table->decimal('mt_total', 10.0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_pedidos');
    }
}
