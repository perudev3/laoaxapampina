<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_transaccion extends Model
{
    protected $table='tbl_transaccion';

    protected $fillable = ['numero'];

    protected $primaryKey = 'id_transaccion';

    public $timestamps = false;

    protected $hidden = ['remember_token'];
}
