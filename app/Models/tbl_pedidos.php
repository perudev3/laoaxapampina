<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_pedidos extends Model
{
    protected $fillable = ['nu_compra', 'token_session', 'id_producto', 'cant_producto', 'no_cliente', 'tl_cliente', 'mail_cliente', 'tx_direccion', 'fe_compra', 'hr_compra', 'tipo_pago', 'est_pedido', 'mt_total'];

    protected $primaryKey = 'id_pedidos';

    public $timestamps = false;

    protected $hidden = ['remember_token'];
}
