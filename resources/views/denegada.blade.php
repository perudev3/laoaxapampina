<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>La Oxapampina</title>
    <meta http-equiv="Content-Type" content="text/html;"/>
    <meta charset="UTF-8">
    <style media="all">
        *{
            margin: 0;
            padding: 0;
            line-height: 1.3;
            font-family: 'Roboto';
            color: #333542;
        }
        body{
            font-size: .875rem;
        }
        .gry-color *,
        .gry-color{
            color:#878f9c;
        }
        table{
            width: 100%;
        }
        table th{
            font-weight: normal;
        }
        table.padding th{
            padding: .5rem .7rem;
        }
        table.padding td{
            padding: .7rem;
        }
        table.sm-padding td{
            padding: .2rem .7rem;
        }
        .border-bottom td,
        .border-bottom th{
            border-bottom:1px solid #eceff4;
        }
        .text-left{
            text-align:left;
        }
        .text-right{
            text-align:right;
        }
        .small{
            font-size: .85rem;
        }
        .currency{

        }
    </style>
</head>
<body>
    <div>
        <div style="background: #eceff4;padding: 1.5rem;">
            <table>
                <tr>
                    <td>
                        <img loading="lazy"  src="{{ asset('logo/01.png') }}" height="100" style="display:inline-block;">
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="font-size: 1.2rem;" class="strong">https://laoxapampina.com</td>
                    <td class="text-right"></td>
                </tr>
                <tr>
                    <td class="gry-color small">Piura,Perú</td>
                </tr>
                <tr>
                    <td class="gry-color small">Telefono de Contacto: +51915001658</td>
                    <td class="text-right small"><span class="gry-color small">E-mail</span> <span class="strong">ventas@laoxapampina.com</span></td>
                </tr>
                <tr>
                    <td class="gry-color small">Fecha: {{$testdate}}</td>
                    <td class="text-right small"><span class="gry-color small">N° de Orden:</span> <span class=" strong">{{$transaccion->numero}}</span></td>
                </tr>
            </table>

        </div>

        <div style="padding: 1.5rem;padding-bottom: 0">
            <table>
                <tr><td class="strong small gry-color">Cliente:</td></tr>
                <tr><td class="strong">{{$no_cliente}}</td></tr>
                <tr><td class="gry-color small">Fecha de Compra: {{$testdate}}</td></tr>
                <tr><td class="gry-color small">Telefono de Contacto: {{$tl_cliente}}</td></tr>
                <tr><td class="gry-color small">Nùmero de Tarjeta: {{ json_encode($responseBody['data']['CARD']) }} </td></tr>
                <tr><td class="gry-color small">Importa de Transacciòn: S/. {{ json_encode($responseBody['data']['AMOUNT']) }} </td></tr>
                <tr><td class="gry-color small">
                Descripcion de Operación: {{ json_encode($responseBody['data']['ACTION_DESCRIPTION']) }}   
                </td></tr>
            </table>
        </div>

    </div>
</body>
</html>
