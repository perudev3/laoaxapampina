@extends('layouts.app')

@section('content')
    
    <router-view name="shopping" :token_session="{{ json_encode($token_session)}}" :hora="{{ json_encode($hora)}}" :transaccion="{{ json_encode($transaccion)}}"></router-view>
    
@endsection