
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Sómos una carnicería nacional fina que ofrece productos nacionales de alta calidad.  Es así, que todos nuestros productos son el resultado de el mayor respeto y cuidado que se le tiene a los animales con los que trabajamos, esto nos permite decir que la carne que producimos y comercializamos, proviene de ganado alimentado con pastos naturales de las pasturas de Oxapampa, sin hormonas ni promotores del crecimiento, brindando a nuestros clientes una carne más tierna, sabrosa y saludable, pensando en su bienestar.">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.html">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'La Oxapampina') }} | Carniceria en Lima - Peru - Carnes de Res, Embutidos, Hamburguesas - Servicio de Delivery</title>
    
    <link href="{{ asset('bootstrap3/css/bootstrap.css') }}" rel="stylesheet">

    <!-- main css -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    
    <!-- mobile css -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    
    <!-- FontAwesome Sup<strong></strong>port -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
    <!-- FontAwesome Support -->
    
    <!-- Btns -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/btn.css') }}" />
    <!-- Btns -->
    
    <!-- Superfish menu -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/superfish/superfish.css') }}" />
    <!-- Superfish menu -->
    
    <!-- Theme Color selector -->
    <link href="{{ asset('js/theme-color-selector/theme-color-selector.css') }}" type="text/css" rel="stylesheet">
    <!-- Theme Color selector -->
    
    <!-- Owl Carousel -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/owl-carousel/owl.carousel.css') }}" />
    <!-- Owl Carousel -->
    
    <!-- Twitter feed -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/twitterfeed.css') }}" />
    <!-- Twitter feed -->
    
    <!-- Typicons -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/typicons/typicons.min.css') }}" />
    <!-- Typicons -->
    
    <!-- WOW animations -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/wow/css/libs/animate.css') }}" />
    <!-- WOW animations -->
    
    <!-- Forms -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/forms.css') }}" />
    <!-- Forms -->
    
    <!-- Flickr feed -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/flickrfeed.css') }}" />
    <!-- Flickr feed -->
    
    <!-- Pulse Slider -->
    <link rel="stylesheet" type="text/css" href="{{ asset('js/pulse/pm-slider.css') }}" />
    <!-- Pulse Slider -->

    <!-- Development Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Cantata+One%7COpen+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!-- Development Google Fonts -->
    <link rel="shortcut icon" href="img/favicon.ico"  />

    <style type="text/css">
        
        /* Animación con keyframe llamada "latidos" */
        @keyframes latidos {
            from { transform: none; }
            50% { transform: scale(1.2); }
            to { transform: none; }
        }
        #page-loader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1000;
            background: #FFF none repeat scroll 0% 0%;
            z-index: 99999;
        }
        #page-loader .corazon{
            position: relative;
            left: 50%;
            top: 50%;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            display: inline-block;
            font-size: 150px;
            text-shadow: 0 0 10px #222,1px 1px  0 #450505;
            color: red;
            animation: latidos .5s infinite;
            transform-origin: center;
        }

        #banner04{
            width:100%;height: 596px !important;
        }


        .whatsapp{
            color: #f3ecec;
            display: block;
            width: 60px;
            height: 60px;
            position: fixed;
            left: 37px;
            background: #3da45b;
            border-radius: 50%;
            line-height: 67px;
            bottom: 8px;
            z-index: 1000;
        }
        .whatsapp .shopcart-icon {
            font-size: 31px;
            position: relative;
            text-transform: uppercase;
            line-height: 61px;
            color: #ffffff;
            margin: 15px;
        }

        .whatsapp .count {
            position: absolute;
            display: inline-block;
            text-align: center;
            width: 22px;
            height: 22px;
            border-radius: 50%;
            top: -3px;
            right: -11px;
            color: #ffffff;
            font-size: 14px;
            line-height: 22px;
        }

        .btn-green{
            background: #464a46;
        }


        #seccion1{
            background-image: url('/logo/parallax.jpg');
        }

        .parallax{
            height:500px;
            background-image:url('/logo/parallax.jpg');
            background-size:cover;
            background-attachment: fixed;
        }

        .imagen_parallax{
            margin-top: 72%;
        }

        .barra_footer{
            padding: 13px 0px 13px 0px;
            background: red;
            margin: 1px;
        }

        .barra_footer2{
            padding: 13px 0px 13px 0px;
            background: red;
            margin: 4px;
        }

        .titulo_footer{
            color:#fff;margin: -19px;margin-top: 0px;
        }

        .titulo_footer2{
            color:#fff;margin: -19px;margin-top: 0px;
        }

        @media (max-width:900px){

            #myCarousel{
                margin-top: 210px !important;
            }

            #logo{
                width: 146px !important;
                margin: 25px !important;
                margin-left: 15px !important;
            }

            .tg-navigation ul li a {
                color: #000000 !important;
            }

            #banner04{
                width:100%;height: 151px !important;
            }

            #redes{
                margin-top: -6px !important;
                height: 89px !important;
                background: rgb(62, 57, 57);
            }

            .imagen_parallax{
                margin-top: 384% !important;
            }

            .barra_footer{
                padding: 19px 0px 3px 5px !important;
                background: red;
                margin: 12px 84px -18px 0px !important;
            }

            .barra_footer2{
                padding: 19px 0px 3px 5px !important;
                background: red;
                margin: 1px 97px -25px 18px !important;
            }

            .titulo_footer{
                color:#fff;margin: -19px;margin: 8px -105px -27px;
            }

            .titulo_footer2{
                color:#fff;margin: -19px;margin: -1px -104px -27px;
            }

            .paralla{
                background-position: center;
            }

            #link{

                margin-left: 66px;

            }
        }

        @media (max-width:450px){

            #myCarousel{
                margin-top: 210px !important;
            }

            #logo{
                width: 146px !important;
                margin: 25px !important;
                margin-left: 15px !important;
            }

            #banner04{
                width:100%;height: 151px !important;
            }

            #redes{
                margin-top: -6px !important;
                height: 89px !important;
                background: rgb(62, 57, 57);
            }

            .imagen_parallax{
                margin-top: 689% !important;
            }

            .titulo_footer{
                color:#fff;margin: -19px;margin: 8px -105px -27px;
            }

            .titulo_footer2{
                color:#fff;margin: -19px;margin: -1px -104px -27px;
            }

            .parallax{
                background-position: center;
            }

            #link{
                margin-left: 66px;
            }
        }

        #leerlibro p,
        #leerlibro ol,
        #leerlibro ul,
        #leerlibro pre,
        #leerlibro blockquote,
        #leerlibro h1,
        #leerlibro h2,
        #leerlibro h3,
        #leerlibro h4,
        #leerlibro h5,
        #leerlibro h6 {
          margin: 0;
          padding: 0;
          counter-reset: list-1 list-2 list-3 list-4 list-5 list-6 list-7 list-8 list-9;
        }
        #leerlibro ol,
        #leerlibro ul {
          padding-left: 1.5em;
        }
        #leerlibro ol > li,
        #leerlibro ul > li {
          list-style-type: none;
        }
        #leerlibro ul > li::before {
          content: '\2022';
        }
        #leerlibro ul[data-checked=true],
        #leerlibro ul[data-checked=false] {
          pointer-events: none;
        }
        #leerlibro ul[data-checked=true] > li *,
        #leerlibro ul[data-checked=false] > li * {
          pointer-events: all;
        }
        #leerlibro ul[data-checked=true] > li::before,
        #leerlibro ul[data-checked=false] > li::before {
          color: #777;
          cursor: pointer;
          pointer-events: all;
        }
        #leerlibro ul[data-checked=true] > li::before {
          content: '\2611';
        }
        #leerlibro ul[data-checked=false] > li::before {
          content: '\2610';
        }
        #leerlibro li::before {
          display: inline-block;
          white-space: nowrap;
          width: 1.2em;
        }
        #leerlibro li:not(.ql-direction-rtl)::before {
          margin-left: -1.5em;
          margin-right: 0.3em;
          text-align: right;
        }
        #leerlibro li.ql-direction-rtl::before {
          margin-left: 0.3em;
          margin-right: -1.5em;
        }
        #leerlibro ol li:not(.ql-direction-rtl),
        #leerlibro ul li:not(.ql-direction-rtl) {
          padding-left: 1.5em;
        }
        #leerlibro ol li.ql-direction-rtl,
        #leerlibro ul li.ql-direction-rtl {
          padding-right: 1.5em;
        }
        #leerlibro ol li {
          counter-reset: list-1 list-2 list-3 list-4 list-5 list-6 list-7 list-8 list-9;
          counter-increment: list-0;
        }
        #leerlibro ol li:before {
          content: counter(list-0, decimal) '. ';
        }
        #leerlibro ol li.ql-indent-1 {
          counter-increment: list-1;
        }
        #leerlibro ol li.ql-indent-1:before {
          content: counter(list-1, lower-alpha) '. ';
        }
        #leerlibro ol li.ql-indent-1 {
          counter-reset: list-2 list-3 list-4 list-5 list-6 list-7 list-8 list-9;
        }
        #leerlibro ol li.ql-indent-2 {
          counter-increment: list-2;
        }
        #leerlibro ol li.ql-indent-2:before {
          content: counter(list-2, lower-roman) '. ';
        }
        #leerlibro ol li.ql-indent-2 {
          counter-reset: list-3 list-4 list-5 list-6 list-7 list-8 list-9;
        }
        #leerlibro ol li.ql-indent-3 {
          counter-increment: list-3;
        }
        #leerlibro ol li.ql-indent-3:before {
          content: counter(list-3, decimal) '. ';
        }
        #leerlibro ol li.ql-indent-3 {
          counter-reset: list-4 list-5 list-6 list-7 list-8 list-9;
        }
        #leerlibro ol li.ql-indent-4 {
          counter-increment: list-4;
        }
        #leerlibro ol li.ql-indent-4:before {
          content: counter(list-4, lower-alpha) '. ';
        }
        #leerlibro ol li.ql-indent-4 {
          counter-reset: list-5 list-6 list-7 list-8 list-9;
        }
        #leerlibro ol li.ql-indent-5 {
          counter-increment: list-5;
        }
        #leerlibro ol li.ql-indent-5:before {
          content: counter(list-5, lower-roman) '. ';
        }
        #leerlibro ol li.ql-indent-5 {
          counter-reset: list-6 list-7 list-8 list-9;
        }
        #leerlibro ol li.ql-indent-6 {
          counter-increment: list-6;
        }
        #leerlibro ol li.ql-indent-6:before {
          content: counter(list-6, decimal) '. ';
        }
        #leerlibro ol li.ql-indent-6 {
          counter-reset: list-7 list-8 list-9;
        }
        #leerlibro ol li.ql-indent-7 {
          counter-increment: list-7;
        }
        #leerlibro ol li.ql-indent-7:before {
          content: counter(list-7, lower-alpha) '. ';
        }
        #leerlibro ol li.ql-indent-7 {
          counter-reset: list-8 list-9;
        }
        #leerlibro ol li.ql-indent-8 {
          counter-increment: list-8;
        }
        #leerlibro ol li.ql-indent-8:before {
          content: counter(list-8, lower-roman) '. ';
        }
        #leerlibro ol li.ql-indent-8 {
          counter-reset: list-9;
        }
        #leerlibro ol li.ql-indent-9 {
          counter-increment: list-9;
        }
        #leerlibro ol li.ql-indent-9:before {
          content: counter(list-9, decimal) '. ';
        }
        #leerlibro .ql-indent-1:not(.ql-direction-rtl) {
          padding-left: 3em;
        }
        #leerlibro li.ql-indent-1:not(.ql-direction-rtl) {
          padding-left: 4.5em;
        }
        #leerlibro .ql-indent-1.ql-direction-rtl.ql-align-right {
          padding-right: 3em;
        }
        #leerlibro li.ql-indent-1.ql-direction-rtl.ql-align-right {
          padding-right: 4.5em;
        }
        #leerlibro .ql-indent-2:not(.ql-direction-rtl) {
          padding-left: 6em;
        }
        #leerlibro li.ql-indent-2:not(.ql-direction-rtl) {
          padding-left: 7.5em;
        }
        #leerlibro .ql-indent-2.ql-direction-rtl.ql-align-right {
          padding-right: 6em;
        }
        #leerlibro li.ql-indent-2.ql-direction-rtl.ql-align-right {
          padding-right: 7.5em;
        }
        #leerlibro .ql-indent-3:not(.ql-direction-rtl) {
          padding-left: 9em;
        }
        #leerlibro li.ql-indent-3:not(.ql-direction-rtl) {
          padding-left: 10.5em;
        }
        #leerlibro .ql-indent-3.ql-direction-rtl.ql-align-right {
          padding-right: 9em;
        }
        #leerlibro li.ql-indent-3.ql-direction-rtl.ql-align-right {
          padding-right: 10.5em;
        }
        #leerlibro .ql-indent-4:not(.ql-direction-rtl) {
          padding-left: 12em;
        }
        #leerlibro li.ql-indent-4:not(.ql-direction-rtl) {
          padding-left: 13.5em;
        }
        #leerlibro .ql-indent-4.ql-direction-rtl.ql-align-right {
          padding-right: 12em;
        }
        #leerlibro li.ql-indent-4.ql-direction-rtl.ql-align-right {
          padding-right: 13.5em;
        }
        #leerlibro .ql-indent-5:not(.ql-direction-rtl) {
          padding-left: 15em;
        }
        #leerlibro li.ql-indent-5:not(.ql-direction-rtl) {
          padding-left: 16.5em;
        }
        #leerlibro .ql-indent-5.ql-direction-rtl.ql-align-right {
          padding-right: 15em;
        }
        #leerlibro li.ql-indent-5.ql-direction-rtl.ql-align-right {
          padding-right: 16.5em;
        }
        #leerlibro .ql-indent-6:not(.ql-direction-rtl) {
          padding-left: 18em;
        }
        #leerlibro li.ql-indent-6:not(.ql-direction-rtl) {
          padding-left: 19.5em;
        }
        #leerlibro .ql-indent-6.ql-direction-rtl.ql-align-right {
          padding-right: 18em;
        }
        #leerlibro li.ql-indent-6.ql-direction-rtl.ql-align-right {
          padding-right: 19.5em;
        }
        #leerlibro .ql-indent-7:not(.ql-direction-rtl) {
          padding-left: 21em;
        }
        #leerlibro li.ql-indent-7:not(.ql-direction-rtl) {
          padding-left: 22.5em;
        }
        #leerlibro .ql-indent-7.ql-direction-rtl.ql-align-right {
          padding-right: 21em;
        }
        #leerlibro li.ql-indent-7.ql-direction-rtl.ql-align-right {
          padding-right: 22.5em;
        }
        #leerlibro .ql-indent-8:not(.ql-direction-rtl) {
          padding-left: 24em;
        }
        #leerlibro li.ql-indent-8:not(.ql-direction-rtl) {
          padding-left: 25.5em;
        }
        #leerlibro .ql-indent-8.ql-direction-rtl.ql-align-right {
          padding-right: 24em;
        }
        #leerlibro li.ql-indent-8.ql-direction-rtl.ql-align-right {
          padding-right: 25.5em;
        }
        #leerlibro .ql-indent-9:not(.ql-direction-rtl) {
          padding-left: 27em;
        }
        #leerlibro li.ql-indent-9:not(.ql-direction-rtl) {
          padding-left: 28.5em;
        }
        #leerlibro .ql-indent-9.ql-direction-rtl.ql-align-right {
          padding-right: 27em;
        }
        #leerlibro li.ql-indent-9.ql-direction-rtl.ql-align-right {
          padding-right: 28.5em;
        }
        #leerlibro .ql-video {
          display: block;
          max-width: 100%;
        }
        #leerlibro .ql-video.ql-align-center {
          margin: 0 auto;
        }
        #leerlibro .ql-video.ql-align-right {
          margin: 0 0 0 auto;
        }
        #leerlibro .ql-bg-black {
          background-color: #000;
        }
        #leerlibro .ql-bg-red {
          background-color: #e60000;
        }
        #leerlibro .ql-bg-orange {
          background-color: #f90;
        }
        #leerlibro .ql-bg-yellow {
          background-color: #ff0;
        }
        #leerlibro .ql-bg-green {
          background-color: #008a00;
        }
        #leerlibro .ql-bg-blue {
          background-color: #06c;
        }
        #leerlibro .ql-bg-purple {
          background-color: #93f;
        }
        #leerlibro .ql-color-white {
          color: #fff;
        }
        #leerlibro .ql-color-red {
          color: #e60000;
        }
        #leerlibro .ql-color-orange {
          color: #f90;
        }
        #leerlibro .ql-color-yellow {
          color: #ff0;
        }
        #leerlibro .ql-color-green {
          color: #008a00;
        }
        #leerlibro .ql-color-blue {
          color: #06c;
        }
        #leerlibro .ql-color-purple {
          color: #93f;
        }
        #leerlibro .ql-font-serif {
          font-family: Georgia, Times New Roman, serif;
        }
        #leerlibro .ql-font-monospace {
          font-family: Monaco, Courier New, monospace;
        }
        #leerlibro .ql-size-small {
          font-size: 0.75em;
        }
        #leerlibro .ql-size-large {
          font-size: 1.5em;
        }
        #leerlibro .ql-size-huge {
          font-size: 2.5em;
        }
        #leerlibro .ql-direction-rtl {
          direction: rtl;
          text-align: inherit;
        }
        #leerlibro .ql-align-center {
          text-align: center;
        }
        #leerlibro .ql-align-justify {
          text-align: justify;
        }
        #leerlibro .ql-align-right {
          text-align: right;
        }
        #leerlibro.ql-blank::before {
          color: rgba(0,0,0,0.6);
          content: attr(data-placeholder);
          font-style: italic;
          left: 15px;
          pointer-events: none;
          position: absolute;
          right: 15px;
        }

        #leerlibro p em{
          font-style: italic;
        }
    </style>

</head>
  
<strong></strong>
<body onload="openDialog();">
<div id="content">
<div id="overlay" class="overlay"></div>

</div>

<!-- Mobile Menu -->
  <div class="pm-mobile-menu-overlay" id="pm-mobile-menu-overlay"></div>
  
  <div class="pm-mobile-global-menu">
                    
    <div class="pm-mobile-global-menu-logo">
        <a href="{{ url('/') }}">
            <img src="{{ asset('logo/LOGO-ORIGINAL-04.png') }}" style="width: 100%;">
        </a> 
    </div>
    
   
    <ul class="sf-menu pm-nav">
                        
        <li><a href="{{ url('/') }}">Home</a></li>
        <li><a href="{{url('/carta')}}">La Carta</a></li>
        <li><a href="{{url('/contactanos')}}">Contáctanos</a></li>
    
    </ul>
        
  </div><!-- /pm-mobile-nav-overlay -->
  
  <!-- Theme color selector --><!-- Theme color selector -->
    

    <div id="pm_layout_wrapper" class="pm-full-mode" ><!-- Use wrapper for wide or boxed mode -->
    
        <!-- Search overlay --><!-- Search overlay end -->
    
        <!-- Sub-header --><!-- /Sub-header -->
    
        <!-- Main navigation -->
        <header>
                
            <div class="container">
            
                <div class="row">
                    
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        
                        <div class="pm-header-logo-container">
                            <a href="{{ url('/') }}">
                                <img src="{{ asset('logo/LOGO-ORIGINAL-04.png') }}" class="img-responsive pm-header-logo" alt="Distribuidora de Carnes Naveda" style="width: 40%;">
                            </a> 
                        </div>
                        
                        <div class="pm-header-mobile-btn-container">
                            <!--<button type="button" class="navbar-toggle pm-main-menu-btn" id="pm-main-menu-btn" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></button>-->
                            <button type="button" class="navbar-toggle pm-main-menu-btn" id="pm-mobile-menu-trigger" ><i class="fa fa-bars"></i></button>
                        </div>
                    
                    </div>
                    
                    <div class="col-lg-8 col-md-8 col-sm-8 pm-main-menu">
                                        
                        <nav class="navbar-collapse collapse">
                        
                            <ul class="sf-menu pm-nav">
                        
                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{url('/carta')}}">La Carta</a></li>
                                <li><a href="{{url('/contactanos')}}">Contáctanos</a></li>
                            
                            </ul>
                        
                        </nav>  
                                              
                    </div>
                    
                </div>
            
            </div>
                    
        </header>
        <!-- /Main navigation -->
                
        <!-- SLIDER AREA -->
        
        <div class="pm-pulse-container" id="pm-pulse-container">
        
            <div id="pm-pulse-loader">
                <img src="{{ asset('js/pulse/img/ajax-loader.gif') }}" alt="slider loading" />
            </div>
            
            <div id="pm-slider" class="pm-slider">
                
                <div id="pm-slider-progress-bar"></div>
            
                <ul class="pm-slides-container" id="pm_slides_container">
                    
                    <!-- FULL WIDTH slides -->
                    <li data-thumb="{{ asset('img/slider/1a.jpg') }}" class="pmslide_0"><img src="{{ asset('img/slider/slide1.jpg') }}" alt="img01" />
                    
                        <div class="pm-holder">
                            <div class="pm-caption">
                                  <h1><span>La Oxapampina</span></h1>
                                  <span class="pm-caption-decription">
                                    ¡Los mejores y más frescos cortes!
                                  </span>
                            </div>
                        </div>
                    
                    </li>
                    
                    <li data-thumb="{{ asset('img/slider/2a.jpg') }}" class="pmslide_1"><img src="{{ asset('img/slider/slide2.jpg') }}" alt="img02" />
                        
                        <div class="pm-holder">
                            <div class="pm-caption">
                                  <h1><span>Productos de calidad</span></h1>
                                  <span class="pm-caption-decription">
                                    ¡Con Seguridad, garantía y responsabilidad!
                                  </span>                                  
                            </div>
                        </div>
                                            
                    </li>
                    
                    <li data-thumb="{{ asset('img/slider/3a.jpg') }}" class="pmslide_2"><img src="{{ asset('img/slider/slide3.jpg') }}" alt="img02" />
                        
                        <div class="pm-holder">
                            <div class="pm-caption">
                                  <h1><span>Las mejores carnes</span></h1>
                                  <span class="pm-caption-decription">
                                     ¡el mejor precio y en la puerta de tu casa o local!
                                  </span>                                  
                            </div>
                        </div>
                                            
                    </li>
                                    
                </ul>
               
            </div>
        
        </div>
        
        <!-- SLIDER AREA end -->
        
        <!-- BODY CONTENT starts here -->
        
        <!-- Overview boxes -->
        

            @if(\Illuminate\Support\Facades\Session::has('envoit-compra'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>{{\Illuminate\Support\Facades\Session::get('envoit-compra')}}
                </div>
            @endif

            <div id="app">

                @yield('content')
            
            </div>
            
            <div class="whatsapp">
                <a class="shopcart-icon" href="https://api.whatsapp.com/send?phone=+51915001658" target="_blank">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </div>
        <!-- Overview boxes end -->
        
        <!-- Menu filter system --><!-- Menu filter system end -->
        
        <!-- Upcoming events filter system -->
      <div class="container pm-containerPadding-top-50 pm-containerPadding-bottom-30">
        <div class="row">
            
          <div class="col-lg-12 pm-containerPadding-bottom-40">
                    
            <div class="pm-featured-header-container">
                    
                        <!-- Featured panel header -->
              <div class="pm-featured-header-title-container events">
                    <p class="pm-featured-header-title">Central telefónica y de Pedidos</p>
                    <p class="pm-featured-header-message">Celulares: +51915001658</p>
                </div>
                <!-- Featured panel header end -->
                        
                <!-- Filter menu --><!-- Filter menu end -->
                    
              </div>
                    
                </div><!-- /.col-lg-12 -->
                
                <!-- Event item --><!-- /.col-lg-4 -->
                <!-- /Event item -->
                
                <!-- Event item --><!-- /.col-lg-4 -->
            <!-- /Event item -->
                
            <!-- Event item --><!-- /.col-lg-4 -->
            <!-- /Event item -->
            
          </div>
        </div>
        <!-- Upcoming events filter system end -->
        
        
        <!-- News filter system --><!-- News filter system end -->
        
        
        <!-- BODY CONTENT end -->

      <footer></footer>
                
        <div class="pm-footer-copyright">
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 pm-footer-copyright-col">
                        <p> © Copyright 2021 - Lima, Perú | Desarrollado por <a href="https://www.facebook.com/LeonardoSamuel.rivas" target="_blank">Samuel Rivas</a></p>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 pm-footer-navigation-col">
                        <ul class="pm-footer-navigation" id="pm-footer-nav">
                            <li><a href="{{url('/')}}">Inicio</a></li>                            
                            <li><a href="{{url('/carta')}}">Tipos de Corte</a></li>
                            <li><a href="{{url('/contactanos')}}">Contáctenos</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
    
    </div><!-- /pm_layout-wrapper -->
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('js/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.viewport.mini.js') }}"></script>
    <script src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('bootstrap3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('js/owl-carousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/jquery.tooltip.js') }}"></script>
    <script src="{{ asset('js/jquery.hoverPanel.js') }}"></script>
    <script src="{{ asset('js/superfish/superfish.js') }}"></script>
    <script src="{{ asset('js/superfish/hoverIntent.js') }}"></script>
    <script src="{{ asset('js/tinynav.js') }}"></script>
    <script src="{{ asset('js/stellar/jquery.stellar.js') }}"></script>
    <script src="{{ asset('js/countdown/countdown.js') }}"></script>
    <script src="{{ asset('js/theme-color-selector/theme-color-selector.js') }}"></script>
    <script src="{{ asset('js/wow/wow.min.js') }}"></script>
    <script src="{{ asset('js/pulse/jquery.PMSlider.js') }}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript" src="https://static-content.vnforapps.com/v2/js/checkout.js"></script>
    <p id="back-top" class="visible-lg visible-md visible-sm"> </p>


  </body>

<!-- Mirrored from www.dcnaveda.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 May 2021 13:29:43 GMT -->
</html>
